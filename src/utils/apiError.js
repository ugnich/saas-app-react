export const getApiError = (err) => {
  const { data, status } = err.response;
  let message = "";

  switch (status) {
    case 400:
      message = "Please check your input and try again";
      break;
    case 403:
      message = "Access denied";
      break;
    case 404:
      message = "The resource you requesting is not found";
      break;
    case 500:
      message = "Server error. Please try again later.";
      break;
    default:
      message =
        "Something is wrong. Please check your input or try again later.";
  }

  return { message, data };
};
