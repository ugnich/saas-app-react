import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-icons/font/bootstrap-icons.css";

import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import {
  ToastsProvider,
  ErrorProvider,
  ErrorMessage,
} from "saas-app-react-components";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";

import reportWebVitals from "./reportWebVitals";
import { interceptors } from "./api/interceptors";
import store from "./store";
import App from "./App";

const persistor = persistStore(store);
// Set up the API interceptors
interceptors(store);

ReactDOM.render(
  <React.StrictMode>
    <ErrorProvider>
      <ToastsProvider>
        <ErrorMessage />
        <BrowserRouter>
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <App />
            </PersistGate>
          </Provider>
        </BrowserRouter>
      </ToastsProvider>
    </ErrorProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
