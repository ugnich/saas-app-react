import React from 'react';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Footer = (props) => {
    return (
        <footer className="mt-5">
            <Container className="text-center">
                <p className="copyright-text text-gray">
                    <Link to="/" className="btn btn-link text-muted text-dark">
                        <small>Footer</small>
                    </Link>
                </p>
            </Container>
        </footer>
    )
}

export default Footer;