import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

const PrivateRoute = ({ children, ...properties }) => {
    const { token } = useSelector((state) => state.auth);

    if (token) {
        return <Route {...properties}>{children}</Route>;
    }

    return <Redirect to='/login' />;
};

export default PrivateRoute;