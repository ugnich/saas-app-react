import '../styles/layout.css';

import React, { useState } from 'react';
import { Container, Row, Nav, NavLink } from 'react-bootstrap';
import { useLocation, Link } from 'react-router-dom';
import { Sidebar } from 'saas-app-react-components';

import Navbar from './Navbar';

const Layout = ({ children }) => {
    const { pathname } = useLocation();
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);

    const handleLayoutClick = (event) => {
        if (isSidebarOpen && !["sidebar-container"].includes(event.target.id)) {
            setIsSidebarOpen(false);
            document.getElementById("root").style.overflow = null;
        }
    };

    const handleNavbarClick = () => {
        setIsSidebarOpen(true);
        document.getElementById("root").style.overflow = "hidden";
    };

    if (["/login", "/signup", "logout"].includes(pathname)) return children;

    return (
        <div className="d-flex min-vh-100" onClick={handleLayoutClick}>
            <Sidebar isOpen={isSidebarOpen}>
                <img src="/logo.png" alt="Logo" className="my-3" />
                <Nav className="flex-column">
                    <NavLink as={Link} to="/logout">
                        Logout
          </NavLink>
                </Nav>
            </Sidebar>
            <div
                className={`d-flex flex-column flex-grow-1 main-content ${isSidebarOpen ? "shifted" : ""
                    }`}
            >
                <Navbar onClick={handleNavbarClick} />
                <main className="flex-grow-1">
                    <Container>
                        <Row>{children}</Row>
                    </Container>
                </main>
            </div>
        </div>
    );
};

export default Layout;
