import '../styles/navbar.css';

import React from 'react';
import { Container, Navbar as BSNavbar, Button } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

const PAGE_TITLES = {};

const Navbar = ({ onClick }) => {
    const { pathname } = useLocation();

    return (
        <BSNavbar bg="light" collapseOnSelect expand="md" className="p-0">
            {!["/login", "/signup", "/logout"].includes(pathname) && (
                <Container fluid className="justify-content-start">
                    <Button
                        variant="link"
                        className="text-decoration-none text-dark sidebar-toggle"
                        onClick={onClick}
                    >
                        <i className="bi bi-list" />
                    </Button>
                    <h2 className="m-0 font-weight-normal">{PAGE_TITLES[pathname]}</h2>
                </Container>
            )}
        </BSNavbar>
    );
};

export default Navbar;