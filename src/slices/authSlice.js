import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import api from "../api";

const initialState = {
  token: null,
  error: null,
  loading: false,
};

export const signUp = createAsyncThunk(
  "auth/signUp",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      return await api.signup({ email, password });
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const login = createAsyncThunk(
  "auth/login",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      return await api.login({ email, password });
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

const authSuccessCallback = (state, { payload }) => {
  const { token } = payload;
  localStorage.setItem("token", token);
  state.token = token;
  state.loading = false;
};

const authErrorCallback = (state, action) => {
  state.error = action.payload ? action.payload : action.error.message;
  state.loading = false;
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    logout: () => {
      localStorage.removeItem("token");
    },
  },
  extraReducers: {
    [signUp.pending]: (state) => {
      state.loading = true;
    },
    [signUp.fulfilled]: authSuccessCallback,
    [signUp.rejected]: authErrorCallback,
    [login.pending]: (state) => {
      state.loading = true;
    },
    [login.fulfilled]: authSuccessCallback,
    [login.rejected]: authErrorCallback,
  },
});

export const authSelector = (state) => state.auth;

export const { logout } = authSlice.actions;

export default authSlice.reducer;
