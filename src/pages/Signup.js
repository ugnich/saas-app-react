import React, { useContext, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Col, Form, Button, Spinner } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import { signUp } from '../slices/authSlice';
import { ErrorContext } from 'saas-app-react-components';

const SignUp = () => {
    const [isTermsChecked, setIsTermsChecked] = useState(false);
    const { setError } = useContext(ErrorContext);
    const { loading, token } = useSelector((state) => state.auth);
    const { push } = useHistory();
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();
    const [passwordMismatchError, setPasswordMismatchError] = useState("");

    useEffect(() => {
        if (!!token) {
            push("/");
        }
    }, [token, push]);

    const onSubmit = async ({ password, passwordConfirm, email, name }) => {
        if (password !== passwordConfirm) {
            setPasswordMismatchError("Password didn't match");
            return;
        }

        if (!isTermsChecked) return;

        const resultAction = await dispatch(signUp({ email, password }));

        const { type, payload } = resultAction;

        if (type === "auth/signup/fulfilled") {
            push("/");
        } else if (type === "auth/signup/rejected") {
            setError(payload);
        }
    };

    return (
        <div
            className="d-flex justify-content-center align-items-center p-3"
            style={{ backgroundColor: "#f0f0f0", minHeight: "100vh" }}
        >
            <Form
                onSubmit={handleSubmit(onSubmit)}
                className="bg-white rounded align-items-center justify-content-center d-flex flex-grow-1 py-5"
                style={{ maxWidth: 500 }}
            >
                <Col
                    xs={12}
                    sm={8}
                    className="flex-column d-flex flex-grow-1 text-center"
                >
                    <img src="/logo.png" alt="Logo" className="mb-5" />
                    <h5 className="font-weight-normal">Sign Up</h5>

                    <Form.Group controlId="formBasicEmail" className="mb-3 text-left">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            {...register("email")}
                            type="email"
                            placeholder="Enter email"
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword" className="mb-3 text-left">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            {...register("password")}
                            type="password"
                            placeholder="Enter password"
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword2" className="mb-3 text-left">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control
                            {...register("passwordConfirm")}
                            type="password"
                            placeholder="Repeat the entered password"
                            required
                        />
                        <Form.Text className="text-danger">
                            {passwordMismatchError}
                        </Form.Text>
                    </Form.Group>
                    <Form.Group className="text-left align-items-center">
                        <Form.Check
                            checked={isTermsChecked}
                            onChange={(event) => setIsTermsChecked(event.target.checked)}
                            type="checkbox"
                            label={
                                <>
                                    I agree to{" "}
                                    <Link to="/tos" target="_blank">
                                        Terms of Service
                                    </Link>{" "}
                                    and{" "}
                                    <Link to="/privacy" target="_blank">
                                        Privacy policy
                                    </Link>
                                </>
                            }
                            required
                        />
                    </Form.Group>

                    <Button
                        variant="success"
                        type="submit"
                        className={`${loading ? "disabled" : ""} mb-3`}
                    >
                        {loading && (
                            <Spinner
                                size="sm"
                                as="span"
                                className="mr-3"
                                animation="border"
                                role="status"
                            >
                                <span className="sr-only">Loading...</span>
                            </Spinner>
                        )}
                        Sign Up
                    </Button>
                    <Link to="/login">Have an account already?</Link>
                </Col>
            </Form>
        </div>
    );
};

export default SignUp;