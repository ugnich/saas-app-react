import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { logout } from '../slices/authSlice';

const Logout = () => {
    const { push } = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(logout());
        push("/");
    }, [dispatch, push]);

    return <></>;
};

export default Logout;