import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Http404 = () => {
    return (
        <Container>
            <Row className="justify-content-center">
                <Col className="text-center">
                    <span className="display-1 d-block">404</span>
                    <div className="mb-4 lead">
                        The page you are looking for was not found.
                    </div>
                    <Link to="/" className="btn btn-link">
                        Back to Home
                    </Link>
                </Col>
            </Row>
        </Container>
    );
};

export default Http404;
