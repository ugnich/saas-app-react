import React, { useContext, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Col, Form, Button, Spinner } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import { login } from '../slices/authSlice';
import { ErrorContext } from 'saas-app-react-components';

const Login = () => {
    const { setError } = useContext(ErrorContext);
    const { push } = useHistory();
    const { loading, token } = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();

    useEffect(() => {
        if (!!token) {
            push("/");
        }
    }, [token, push]);

    const onSubmit = async ({ email, password }) => {
        const resultAction = await dispatch(login({ email, password }));
        const { type, payload } = resultAction;

        if (type === "auth/login/fulfilled") {
            push("/");
        } else if (type === "auth/login/rejected") {
            setError(payload);
        }
    };

    return (
        <div
            className="d-flex justify-content-center align-items-center p-3"
            style={{ backgroundColor: "#f0f0f0", minHeight: "100vh" }}
        >
            <Form
                onSubmit={handleSubmit(onSubmit)}
                className="bg-white rounded align-items-center justify-content-center d-flex flex-grow-1 py-5"
                style={{ maxWidth: 500 }}
            >
                <Col
                    xs={12}
                    sm={8}
                    md={6}
                    className="flex-column d-flex flex-grow-1 text-center"
                >
                    <img src="/logo.png" alt="Logo" className="mb-5" />
                    <h5 className="font-weight-normal">Log in</h5>
                    <Form.Group controlId="formBasicEmail" className="mb-3 text-left">
                        <Form.Label className="mb-0">Email</Form.Label>
                        <Form.Control
                            {...register("email")}
                            type="email"
                            placeholder="Enter email"
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword" className="text-left">
                        <Form.Label className="mb-0">Password</Form.Label>
                        <Form.Control
                            {...register("password")}
                            type="password"
                            placeholder="Enter password"
                            required
                        />
                    </Form.Group>
                    <Button
                        variant="success"
                        type="submit"
                        className={`${loading ? "disabled" : ""} mb-3`}
                    >
                        {loading && (
                            <Spinner
                                size="sm"
                                as="span"
                                className="mr-3"
                                animation="border"
                                role="status"
                            >
                                <span className="sr-only">Loading...</span>
                            </Spinner>
                        )}
                        Login
                    </Button>
                    <Link to="/">Forgot password</Link>
                    <Link to="/signup">Create new account</Link>
                </Col>
            </Form>
        </div>
    );
};

export default Login;
