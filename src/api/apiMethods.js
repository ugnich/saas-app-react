import instance from "./axiosInstance";
import { getApiError } from "../utils/apiError";

// HTTP GET Request
export const get = async (path) => {
  try {
    return await instance.get(path);
  } catch (error) {
    throw getApiError(error);
  }
};

// HTTP POST Request
export const post = async (path, data) => {
  try {
    return await instance.post(path, data);
  } catch (error) {
    throw getApiError(error);
  }
};

// HTTP PUT Request
export const put = async (path, data) => {
  try {
    return await instance.put(path, data);
  } catch (error) {
    throw getApiError(error);
  }
};

// HTTP PATCH Request
export const patch = async (path, data) => {
  try {
    return await instance.patch(path, data);
  } catch (error) {
    throw getApiError(error);
  }
};

// HTTP DELETE Request
export const del = async (path) => {
  try {
    return await instance.delete(path);
  } catch (error) {
    throw getApiError(error);
  }
};
