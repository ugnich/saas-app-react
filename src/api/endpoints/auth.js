import { post } from "../apiMethods";

const signup = async ({ email, password }) => {
  const { data } = await post("auth/signup/", { email, password });
  return data;
};

const login = async ({ email, password }) => {
  const { data } = await post("auth/login/", {
    email,
    password,
  });
  return data;
};

const authApi = { signup, login };

export default authApi;
