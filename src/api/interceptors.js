import instance from "./axiosInstance";
import { logout } from "../slices/authSlice";

export const interceptors = (store) => {
  instance.interceptors.request.use((config) => {
    const token = localStorage.getItem("token");
    if (!!token) {
      config.headers.authorization = `Bearer ${token}`;
    }

    return config;
  });
  instance.interceptors.response.use(
    (res) => {
      return res;
    },
    (error) => {
      const { status } = error.response;
      if (status === 401) {
        const { dispatch } = store;
        dispatch(logout());
      }
      return Promise.reject(error);
    }
  );
};
