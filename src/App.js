import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './components/Layout';
import Login from './pages/Login';
import SignUp from './pages/Signup';
import Logout from './pages/Logout';
import Http404 from './pages/Http404';
import PrivateRoute from './components/PrivateRoute';

function App() {
    return (
        <div className="App">
            <Layout>
                <Switch>
                    <PrivateRoute path="/" exact>
                        <h1>Home Page</h1>
                    </PrivateRoute>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/signup">
                        <SignUp />
                    </Route>
                    <Route path="/logout">
                        <Logout />
                    </Route>
                    <Route path="/">
                        <Http404 />
                    </Route>
                </Switch>
            </Layout>
        </div>
    );
}

export default App;
